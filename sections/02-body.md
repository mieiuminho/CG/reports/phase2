# Logic

We have 3 main data structures: a matrix of points (`float *`) (`__models__`)
that stores the points of each model that is referenced in the configuration
file. We have a vector of file names (`__files__`) that stores the name of the
_.3d_ files of every referenced model. Finally, we have a vector of actions
(`__actions__`) that stores every action that is referenced in the input, may
that be: a translate, scale or rotate.

The main idea is to go through the input file and and parse every group. We can
think of the `.xml` file as a N-Ary Tree that we go through using a depth-first
algorithm, since we go through every group and get every element of it (if the
element has sub-elements we get them too in the `parseGroup` function).

```c++
std::vector<char*> parse(const char* filePath, std::vector<const char*>* files){
    std::vector<char*> actions;
    XMLDocument config;
    config.LoadFile(filePath);


    XMLElement* scene = config.FirstChildElement("scene");
    XMLElement* group = scene->FirstChildElement("group");

    while(group){
        parseGroup(group,files,&actions);
        group = group->NextSiblingElement("group");
    }
    return actions;

}
```

As you can see in the previous segment of code: for every group, we go through
it and parse the actions that are referenced. Every action can be of the
following type: "translate", "rotate", "scale" and possibly refer more models
with the "models". The mentioned possibilities are covered in:

```c++
void parseGroup(XMLElement *group, std::vector<const char *> *files,
                std::vector<char *> *actions) {
    XMLElement *elem = group->FirstChildElement();

    actions->push_back(strdup("PUSH"));

    while (elem) {

        const char *name = elem->Name();

        if (strcmp(name, "group") == 0) {
            parseGroup(elem, files, actions);

        } else if (strcmp(name, "translate") == 0) {
            actions->push_back(parseTrans(elem));

        } else if (strcmp(name, "rotate") == 0) {
            actions->push_back(parseRotate(elem));

        } else if (strcmp(name, "scale") == 0) {
            actions->push_back(parseScale(elem));

        } else if (strcmp(name, "models") == 0) {
            parseModels(elem, files, actions);
       }
        elem = elem->NextSiblingElement();
    }

    actions->push_back(strdup("pop"));
}
```

We store a "PUSH" action in the `__actions__` vector in order to preserve the
coordenate axis state in between actions. We store a "POP" action after for the
same reason.


Finally, inside every action we parse it's arguments using the following
functions:

* `parseTrans(XMLElement *elem)` - To parse the translation arguments.
* `parseRotate(XMLElement *elem)` - To parse the rotate arguments.
* `parseScale(XMLElement *elem)` - To parse the scale arguments.
* `parseModels(XMLElement *elem, vector<const char*> *files, vector<char *> actions)` - To parse the referenced models inside a group.


# Demo Scene

![Solar System Demo](figures/solar_system.png)


