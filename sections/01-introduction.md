# Introduction {#sec:introduction}

The goal of the assignement is to develop a mini scene graph based 3D engine
and provide usage examples that show its potential. The assignement is split
in four phases, each with a due date.

This report is related with 2nd phase: this phase is about creating hierarchical
scenes using geometric transforms. A scene is defined as a tree where each node
contains a set of geometric transforms (translate, rotate and scale) and
optionally a set of models. Each node can also have children nodes.

Geometric transformations can only exist inside a group, and are applied to all
models and subgroups.

\newpage

